const cnt = document.getElementById("conte");
let codigo;
var descrip = [];
let eMap = new Map();
let nn;


async function leerJSON(url) {
    try {
        let response = await fetch(url);
        let user = await response.json();
        return user;
    } catch (err) {
        alert(err);
    }
}


function takeData() {
    const urlData = window.location.search.substr(1);
    let s = urlData.split("&");
    const query = [];
    for (let i = 0; i < s.length; i++) {
        let aux = s[i].split("=");
        query.push(parseInt(aux[1]));
    }
    
    
    let ids = eliminarNotas(query);
    cnt.innerHTML = eMap.get(query[0]);
    const notas = eMap.get(query[0]).notas;
    for (let i = 0; i < notas; i++) {
        if (ids(notas[i].id)) {
            notas[i]["observacion"] = "Nota Eliminada";
        }
        else {
            notas[i]["observacion"] = (notas[i].valor < 3 ? "Nota Reprobada" : "Nota Aprobada");
        }
    }
    buildTable();
}

function eliminarNotas(query) {
    const notas = eMap.get(query[0]).notas;
    
    notas.sort(function (a, b) { return a.valor - b.valor; });
    
    const ids = new Set();
    for (let i = 0; i < query[1]; i++) {
        ids.add(parseInt(notas[i].id));
    }
    return ids;
}

function buildTable(codigo) {
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Descripción');
    data.addColumn('number', 'Valor');
    data.addColumn('string', 'Observación');
    let notas = eMap.get(codigo).notas;
    for (let i = 0; i <= notas.length; i++) {
        let index = parseInt(notas[i].id) - 1;
        data.addRow([descrip[index].descripcion, notas[i].valor, notas[i].observacion]);
    }
    var table = new google.visualization.Table(document.getElementById('table_div'));
    table.draw(data, { showRowNumber: false, width: '25%', height: '100%' });
}

document.addEventListener("DOMContentLoaded", () => {
    let url = "https://programacion-web---i-sem-2019.gitlab.io/persistencia/json_web/json/estudiantes.json";
    leerJSON(url).then(
        datos => {
            nn = datos.nombreMateria;
            for (let i = 0; i < datos.estudiantes.length; i++) {
                let E = datos.estudiantes[i];
                let obj = {
                    name: E.nombre,
                        notas: E.notas
                };
                eMap.set(E.codigo, obj);
            }
            descrip = datos.descripcion;
            cnt.innerHTML = eMap.get(115100).notas.toString();
        }
    );
});
