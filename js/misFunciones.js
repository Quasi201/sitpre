const myMap = new Map();
const btnNotas = document.getElementById("consulta");

async function leerJSON(url) {
    try {
        let response = await fetch(url);
        let user = await response.json();
        return user;
    } catch (err) {
        alert(err);
    }
}

function init(){
    let url = "https://programacion-web---i-sem-2019.gitlab.io/persistencia/json_web/json/estudiantes.json";
    leerJSON(url).then(
        datos =>{
            for(let i=0; i<datos.estudiantes.length; i++){
                let E = datos.estudiantes[i];
                myMap.set(E.codigo, E.notas.length);
            }
        }
    );
}

function pass(codigo, eliminar){
    const numeroNotas = myMap.get(codigo);
    if(numeroNotas === undefined){
        return "No hay ningun estudiante asociado a este codigo, porfavor verificar";
    }
    if(eliminar<0 || eliminar>numeroNotas){
        return "El numero de notas es invalido";
    }
    return true;
}

btnNotas.addEventListener('click', ()=>{
    const codigo = parseInt( document.getElementById("codigo").value ); 
    const eliminar = parseInt( document.getElementById("eliminar").value ); 
    const cmd = pass(codigo, eliminar);
    if(cmd === true){
        location.href  = 'html/mostrarNotas.html?codigo='+codigo+'&eliminar='+eliminar;
    }
    else{
        alert(cmd);
    }
});

document.addEventListener("DOMContentLoaded",  ()=> {
    init();
});